angular
    .module('app', ['offline', 'angular-cache'])
    .run(function ($http, $cacheFactory, CacheFactory, offline) {
        $http.defaults.cache = $cacheFactory('custom');
        offline.stackCache = CacheFactory.createCache('my-cache', {
            storageMode: 'localStorage'
        });

        offline.start($http);
    });