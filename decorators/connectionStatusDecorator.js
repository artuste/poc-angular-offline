angular
    .module('app')
    .config(function (offlineProvider, $provide) {
        $provide.decorator('connectionStatus', function ($delegate) {
            $delegate.online = true;
            $delegate.isOnline = function () {
                return $delegate.online;
            };
            return $delegate;
        });
        offlineProvider.debug(true);
    });