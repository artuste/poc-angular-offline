(function () {
    'use strict';

    angular.module('app')
        .directive('mainComponent', mainComponent);
    function mainComponent() {
        return {
            restrict: 'EA',
            scope: {},
            controller: Controller,
            templateUrl: 'components/main/main.html'
        };

        Controller.$inject = ['$scope', '$http', '$log', 'offline', 'connectionStatus'];


        function Controller($scope, $http, $log, offline, connectionStatus) {
            $scope.toggleOnline = function () {
                connectionStatus.online = !connectionStatus.online;
                offline.processStack();
            };

            $scope.isOnline = function () {
                return connectionStatus.isOnline();
            };

            $scope.makeGET = function () {
                $http.get('/get/test', {offline: true})
                    .then(function (response) {
                        $log.info('GET RESULT', response.data);
                    }, function (error) {
                        $log.info('GET ERROR', error);
                    });
            };

            $scope.makePOST = function () {
                $http.post('/post/test', {}, {offline: true})
                    .then(function (response) {
                        $log.info('POST RESULT', response);
                    }, function (error) {
                        $log.info('POST ERROR', error);
                    });
            };

        }
    }
})();